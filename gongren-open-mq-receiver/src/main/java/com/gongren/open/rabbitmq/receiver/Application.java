package com.gongren.open.rabbitmq.receiver;

import com.dlg.commons.constant.MqKeyConstants;
import org.springframework.amqp.core.Queue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class},scanBasePackages = {"com.gongren"})
@ImportResource(value = {"classpath:gongren-open-mq-reveiver.xml"})
public class Application {

    @Bean
    public Queue registerQueue() {
        return new Queue(MqKeyConstants.activityRegister);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }
}