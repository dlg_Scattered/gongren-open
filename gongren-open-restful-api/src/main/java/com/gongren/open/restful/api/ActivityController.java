package com.gongren.open.restful.api;

import com.gongren.project.open.rpc.api.IActivityRpcService;
import jdk.nashorn.internal.ir.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by zhangjinxia on 2017/8/8.
 */
@RestController
@RequestMapping("/activity")
public class ActivityController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Reference
    private IActivityRpcService activityRpcService;

    @RequestMapping("/register")
    @ResponseBody
    public Boolean register() {
//        activityRpcService.peacock(null, 3);
        return true;
    }
}
