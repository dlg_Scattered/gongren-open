package com.gongren.open.rpc.service.redis;

import org.hibernate.validator.constraints.NotBlank;

import java.util.Set;

/**
 * Created by jqwang on 15/10/21.
 */
public interface CacheService {

    void remove(@NotBlank final String key);

    void putObject(@NotBlank final String key, @NotBlank Object fieldVal, Long expireTime);

    void putObject(@NotBlank final String key, @NotBlank Object fieldVal);

    void putObjectPermanent(@NotBlank final String key, @NotBlank final Object value);

    <T> T getObject(@NotBlank final String key, Class<T> clazz);

    Boolean existsKey(@NotBlank String key);

    Long getExpire(@NotBlank String key);

    void remove(@NotBlank String[] keys);

    Integer removePattern(@NotBlank String pattern);

    Set<String> getKeys(@NotBlank String pattern);

    Long leftPush(@NotBlank final String key, @NotBlank final Object value, Long expireTime);

    Long leftPush(@NotBlank final String key, @NotBlank final Object value);

    Long rightPush(@NotBlank final String key, @NotBlank final Object value, Long expireTime);

    Long rightPush(@NotBlank final String key, @NotBlank final Object value);

    <T> T leftPop(@NotBlank final String key, Class<T> clazz);

    <T> T rightPop(@NotBlank final String key, Class<T> clazz);

    <T> T indexPop(@NotBlank final String key, @NotBlank final Long index, Class<T> clazz);

    Long listSize(@NotBlank final String key);

    Boolean  setNx(final String key, final String value);

    String getSet(final String key, final String value);

}
