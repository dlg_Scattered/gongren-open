package com.gongren.open.rpc.service.dao.mappers;

import com.gongren.open.rpc.service.dao.annotation.ActivityRepository;
import com.gongren.open.rpc.service.domain.ActivitySignUp;
import org.apache.ibatis.annotations.Param;

/**
 * Created by zhangjinxia on 2017/9/6.
 */
@ActivityRepository
public interface ActivitySignUpMapper {
    void save(@Param("act") ActivitySignUp activitySignUp);
}
