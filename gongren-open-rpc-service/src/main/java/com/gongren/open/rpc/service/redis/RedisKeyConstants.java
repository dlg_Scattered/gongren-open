package com.gongren.open.rpc.service.redis;

/**
 * Created by zhangjinxia on 2017/9/7.
 */
public class RedisKeyConstants {
    public static final String ACTIVITY_REGISTER_PHONE = "activity:register:phone:";

    public static final String ACTIVITY_REGISTER_IDCARD = "activity:register:idcard:";

    public static final String ACTIVITY_REGISTER_CANAL = "activity:register:canal:";

    public static final String ACTIVITY_PEACOCK = "activity:peacock:";

}
