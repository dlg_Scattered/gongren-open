package com.gongren.open.rpc.service.domain;

import com.dlg.commons.constant.DigitConstant;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 * Created by zhangjinxia on 2017/9/6.
 */
public class ActivitySignUp implements Serializable {

    private BigInteger id;

    /**
     * 0已删除 1有效
     */
    private Short active = DigitConstant.SHORT_ONE;

    /**
     * 数据版本
     */
    private Long version;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建人Id
     */
    private BigInteger createUserId;

    /**
     * 更新时间
     */
    private Date modifyTime;

    /**
     * 更新人Id
     */
    private BigInteger modifyUserId;

    private String name;

    private String tel;

    private String idcard;

    private String canalNumber;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public Short getActive() {
        return active;
    }

    public void setActive(Short active) {
        this.active = active;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public BigInteger getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(BigInteger createUserId) {
        this.createUserId = createUserId;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public BigInteger getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(BigInteger modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getCanalNumber() {
        return canalNumber;
    }

    public void setCanalNumber(String canalNumber) {
        this.canalNumber = canalNumber;
    }
}
