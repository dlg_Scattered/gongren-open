package com.gongren.open.rpc.service.mq;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dlg.commons.constant.MqKeyConstants;
import org.apache.log4j.Logger;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * Created by zhangjinxia on 2017/8/10.
 */
@Component
public class MqReceiver {

    private static Logger logger = Logger.getLogger(MqReceiver.class);

//    @Reference
//    private IActivitySignUpRpcService activitySignUpRpcService;
//
//    @RabbitListener(queues = MqKeyConstants.register)
//    public void register(ActivitySignUpRpcVo activitySignUpRpcVo) {
//        logger.info(String.format("receive register activity register: %s", activitySignUpRpcVo));
//        activitySignUpRpcService.saveActivitySignUp(activitySignUpRpcVo);
//    }
}
