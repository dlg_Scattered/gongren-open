package com.gongren.open.rpc.service.mq;

import com.dlg.commons.constant.MqKeyConstants;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

/**
 * Created by zhangjinxia on 2017/8/10.
 */
@Service
public class MqSender implements IMqSender {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    @Bean
    public Queue registerQueue() {
        return new Queue(MqKeyConstants.register);
    }
    @Bean
    public Queue insertEnterpriseAgentQueue() {
        return new Queue(MqKeyConstants.insertEnterpriseAgent);
    }
    @Bean
    public Queue syncAccountToOrangeQueue() {
        return new Queue(MqKeyConstants.syncAccountToOrange);
    }
    @Bean
    public Queue updateEnterpriseAgentQueue() {
        return new Queue(MqKeyConstants.updateEnterpriseAgent);
    }
    @Bean
    public Queue updateEnterpriseAttributeQueue() {
        return new Queue(MqKeyConstants.updateEnterpriseAttribute);
    }
    @Bean
    public Queue updatePersonalAttributeQueue() {
        return new Queue(MqKeyConstants.updatePersonalAttribute);
    }

    @Override
    public void send(String queue, Object o) {
        rabbitTemplate.convertAndSend(queue, o);
    }
}
