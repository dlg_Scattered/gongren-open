package com.gongren.open.rabbitmq.sender;

/**
 * Created by zhangjinxia on 2017/8/10.
 */
public interface IMqSender {

    void send(String key, Object o);

}
